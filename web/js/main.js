$('.create_overhead').click(function(e) {
    e.preventDefault();
    $.get('/overhead/create', {}, function (data) {
            $('.modal-body').html(data);
            $('#overhead_modal').modal();

            $("#create_form").on("submit", function (e) {
                    e.preventDefault();
                    var form = $(this);
                    $.ajax({
                        url: "/overhead/create",
                        type: "POST",
                        data: form.serialize(),
                        success: function (result) {
                            if (result.status == 'success') {
                                $.pjax.reload({container: '#table-grid-pjax'});
                                $('#overhead_modal').modal('hide');
                            }
                        }
                    });
                }
            );
        }
    );
});


function updateOverhead(id)
{
    $.get('/overhead/update', {'id': id}, function (data) {
            $('.modal-body').html(data);
            $('#overhead_modal').modal();

            $("#update_form").on("submit", function (e) {
                var data = $(this).serialize();
                data.id = id;
                $.ajax({
                    url: '/overhead/update?id='+id,
                    type: "POST",
                    data: data,
                    success: function (result) {
                        if (result.status == 'success') {
                            $.pjax.reload({container: '#table-grid-pjax'});
                            $('#overhead_modal').modal('hide');
                        }
                    }
                });
                e.preventDefault();
            });
        }
    );


}


