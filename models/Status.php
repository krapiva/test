<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status".
 *
 * @property int $id
 * @property string $title Статус
 *
 * @property Overhead[] $overheads
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'title' => Yii::t('app','Статус'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOverheads()
    {
        return $this->hasMany(Overhead::className(), ['status_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getStatusList()
    {
        $status = self::find()->all();
        return ArrayHelper::map($status, 'id', 'title');
    }
}
