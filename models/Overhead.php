<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "overhead".
 *
 * @property int $id
 * @property string $city_out Город отправки
 * @property string $city_in Город доставки
 * @property string $recipient Получатель
 * @property int $status_id Статус
 *
 * @property Status $status
 */
class Overhead extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'overhead';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_out', 'city_in', 'recipient', 'status_id'], 'required'],
            [['status_id'], 'integer'],
            [['city_out', 'city_in', 'recipient'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','ID'),
            'city_out' => Yii::t('app','Откуда'),
            'city_in' => Yii::t('app','Куда'),
            'recipient' => Yii::t('app','Получатель'),
            'status_id' => Yii::t('app','Статус'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}
