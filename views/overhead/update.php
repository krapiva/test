<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\models\Status;

/* @var $this yii\web\View */
/* @var $model app\models\Overhead */

$this->title = 'Обновить накладную №' . $model->id;

?>
<div class="overhead-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php $form = ActiveForm::begin([
        'id' => 'update_form',
        'enableAjaxValidation' => true,
        'validationUrl' => Url::toRoute(['overhead/validate'])
    ]); ?>

    <?= $form->field($model, 'city_out')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_in')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipient')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_id')->dropDownList(Status::getStatusList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
