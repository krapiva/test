<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OverheadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Накладные';
?>
<div class="overhead-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Создать', ['class' => 'btn btn-success create_overhead']) ?>
    </p>
    <?php Pjax::begin(['id' => 'table-grid-pjax']) ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'city_out',
                'city_in',
                'recipient',
                [
                  'attribute' => 'status_id',
                  'value' => function($data) {
                    return $data->status->title;
                  }
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model) {
                            return Html::a("<span class='glyphicon glyphicon-pencil'></span>", '', [
                                'title' => Yii::t('app', 'view'),
                                'onclick' => 'updateOverhead(' . $model->id . ')'
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    <?php Pjax::end() ?>
</div>
<?php  Modal::begin(['id' => 'overhead_modal']) ?>
<?php Modal::end() ?>
