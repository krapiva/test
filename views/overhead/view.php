<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Overhead */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Overheads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="overhead-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'sity_out',
            'sity_in',
            'recipient',
            'status_id',
        ],
    ]) ?>

</div>
