<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Overhead */

$this->title = 'Создать накладную';

?>
<div class="overhead-create">

    <h1><?= Html::encode($this->title) ?></h1>
        <?php $form = ActiveForm::begin([
                'id' => 'create_form',
                'enableAjaxValidation' => true,
                'validationUrl' => Url::toRoute(['overhead/validate'])
        ]); ?>

        <?= $form->field($model, 'city_out')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'city_in')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'recipient')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'status_id')->dropDownList(Status::getStatusList()) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
