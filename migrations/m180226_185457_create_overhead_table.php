<?php

use yii\db\Migration;

/**
 * Handles the creation of table `overhead`.
 */
class m180226_185457_create_overhead_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('overhead', [
            'id' => $this->primaryKey(),
            'city_out' => $this->string()->comment('Город отправки'),
            'city_in' => $this->string()->comment('Город доставки'),
            'recipient' => $this->string()->comment('Получатель'),
            'status_id' => $this->integer()->comment('Статус')
        ]);

        $this->createTable('status', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->comment('Статус'),
        ]);

        $this->createIndex('idx-overhead_id-overhead', 'overhead', 'id', true);
        $this->addForeignKey('fk-status_id-overhead', 'overhead', 'status_id', 'status', 'id');

        $this->batchInsert('status', ['title'], [
            ['Ожидает отправки'],
            ['Доставлено'],
            ['В пути'],
            ['Принят на склад'],
            ['Возвращен'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-status_id-overhead', 'overhead');
        $this->dropIndex('idx-overhead_id-overhead', 'overhead');
        $this->dropTable('overhead');
        $this->dropTable('status');
    }
}
